The experimental CT and pressure data for the study "Experimental characterization of H2/water multiphase flow in heterogeneous sandstone rock at the core scale relevant for underground hydrogen storage (UHS)" by Maartje Boon and Hadi Hajibeygi can be found here. 

Please contact Dr. Maartje Boon at m.m.boon@tudelft.nl if you have questions about the data.
